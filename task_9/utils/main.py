import os

from task_9.utils.new_alg import Calculations


def greetings():
    print("Полиномиальное интерполирование значений функции с заданным аргументом")
    print("Выполнили:")
    print("\tРябков Алексей")
    print("\tФилимонов Александр")
    print("\tЯппаров Артем")


def get_available_num():
    list = os.listdir(os.getcwd())  # dir is your directory path
    count = 0
    for x in list:
        if "test_" in x:
            count += 1
    return count


def get_available():
    list = os.listdir(os.getcwd())  # dir is your directory path
    answer = []
    for x in list:
        if "test_" in x and "result" not in x:
            answer.append(x)
    print(answer)
    return answer


def is_file_available(name):
    list = os.listdir(os.getcwd())
    print(name)
    if name in list:
        return True
    return False


def pick_the_file():
    print("В данный момент в системе доступно", get_available_num(), "тестовых файлов")
    file = input("Введите номер теста (нумерация с 0): ")
    if is_file_available("test_" + file + ".txt"):
        return file
    print("Увы, такой файл недоступен")
    return ""


def main():
    greetings()
    calculator = Calculations()
    while True:
        print("Выберите способ ввода данных:")
        print("\t1. Выбрать среди тестовых файлов")
        print("\t2. Просчитать все тестовые файлы")
        print("\t3. Считать из вашего нетестового файла")
        choice = input("> ")
        if choice == "1":
            ans = pick_the_file()
            if ans != "":
                calculator.initialize_with(file="test_" + ans + ".txt", write_at="result_test_" + ans + ".txt")
                calculator.calculations()
        elif choice == "2":
            ans = get_available()
            for file in ans:
                print("Тестовый файл", file, end=":   ")
                calculator.initialize_with(file=file, write_at="result_" + file)
                calculator.calculations()
        elif choice == "3":
            file = input("Введите название вашего файла (с учетом его расширения): ")
            if is_file_available(file):
                calculator.initialize_with(file=file, write_at="result_" + file)
                calculator.calculations()
            else:
                print("Увы, такой файл недоступен")
        elif choice == "0":
            return
        else:
            print("Введена некорректная команда")
        print("")
        print("=========================================================================")
        print("")


main()
