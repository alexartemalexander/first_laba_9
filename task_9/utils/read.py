
def string_to_list(vector_line):
    vector_line = vector_line.replace('\n', "")
    vector_line = vector_line.split()

    vector = []

    for x in vector_line:
        vector.append(float(x))

    return vector


def reading_file(file_name):
    the_file = open(file_name)

    vector_x_line = ""
    vector_y_line = ""
    eps_line = ""
    XX_line = ""

    for line in the_file:
        if "[EPS]: " in line:
            eps_line = line.replace("[EPS]: ", "")
        elif "[XX]: " in line:
            XX_line = line.replace("[XX]: ", "")
        elif "[N]: " in line:
            N_line = line.replace("[N]: ", "")
        elif "[vectorX]: " in line:
            vectorx_line = line.replace("[vectorX]: ", "")
            vector_x = string_to_list(vectorx_line)
        elif "[vectorY]: " in line:
            vectory_line = line.replace("[vectorY]: ", "")
            vector_y = string_to_list(vectory_line)
        else:
            print("Something went wrong")
            return None

    return {
        "eps": float(eps_line),
        "xx": float(XX_line),
        "vector_x": vector_x,
        "vector_y": vector_y,
        "n": int(N_line)
    }

#
# reading_file("test_0.txt")
