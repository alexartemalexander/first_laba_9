from errors import not_enough_error, not_included_error, not_sorted_error, Error

# Валидируем входные данные


# проверка на упорядоченность аргументов
def sorted(the_array):
    if len(the_array) == 0 or len(the_array) == 1:
        return True
        
    array_length = len(the_array)
    
    for index in range(0, array_length - 1):
        if the_array[index] > the_array[index + 1]:
            return False
    
    return True


# проверка на дурака (если дурак(true) - плохо)
def dumb(count, vectorX, vectorY):
    if count < 0:
        return True
    if len(vectorX) != count or len(vectorY) != count:
        return True
    return False

    
# проверка на достаточность элементов в таблице
def enough(count):
    #TODO: удостовериться, что этого условия достаточно (нет, его не достаточно)
    if count < 2:
        return False
    return True


# проверка на вхождение XX в вектор Х
def included(number, vector):
    if len(vector) < 1:
        return False
    if number < vector[0]:
        return False
    if number > vector[len(vector) - 1]:
        return False
    return True

    
# главная функция проверки
def validated(number, count, vectorX, vectorY):
    if dumb(count=count, vectorX=vectorX, vectorY=vectorY):
        return Error("", 'Проверьте входные данные')
    # if not enough(count=number):
    #     return not_enough_error()
    if not sorted(vectorX):
        return not_sorted_error()
    if not included(number=number, vector=vectorX):
        return not_included_error()
    return "Norm"

# print(validated(5, [1,1,1,2], [0,0,1,2,3]))
    
