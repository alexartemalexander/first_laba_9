errors = {
    'awesome': {
        'code': "IER 0",
        'message': 'Нет ошибки, требуемая точность достигнута'
    },
    'weakest_accuracy': {
        'code': "IER 1",
        'message': 'Требуемая точность не достигнута (N мало)'
    },
    'weak_accuracy': {
        'code': "IER 2",
        'message': 'Требуемая точность не достигается. Модуль разности между двумя последовательными интерполяционными значениями начал расти'
    },
    'not_sorted': {
        'code': "IER 3",
        'message': 'В векторе X нарушен порядок возрастания аргументов'
    },
    'missed_arg': {
        'code': "IER 4",
        'message': 'Значение аргумента XX не принадлежит отрезку'
    }
}


def not_sorted_error():
    cur_error_dict = errors['not_sorted']
    return Error(cur_error_dict['code'], cur_error_dict['message'])


def not_enough_error():
    cur_error_dict = errors['weakest_accuracy']
    return Error(cur_error_dict['code'], cur_error_dict['message'])


def not_included_error():
    cur_error_dict = errors['missed_arg']
    return Error(cur_error_dict['code'], cur_error_dict['message'])


def no_errors():
    cur_error_dict = errors['awesome']
    return Error(cur_error_dict['code'], cur_error_dict['message'])


def week_accurancy():
    cur_error_dict = errors['weak_accuracy']
    return Error(cur_error_dict['code'], cur_error_dict['message'])


class Error:
    
    def __init__(self, error_code, error_message):
        self.error_code = error_code
        self.error_message = error_message
    
    def __str__(self):
        return 'IER = {} - {}'.format(self.error_code, self.error_message)
