from task_9.utils import validator
from task_9.utils.read import *
from task_9.utils.errors import *


class Calculations:
    N = 0
    #X = [1.0, 1.05, 1.1, 1.15, 1.2, 1.25]
    #X = [0, 0.5235987755982988, 0.7853981633974483, 1.0471975511965976, 1.5707963267948966, 3.141592653589793]
    #Y = [2.718281828459045, 2.857651118063164, 3.0041660239464334, 3.158192909689767, 3.3201169227365472, 3.4903429574618414]
    #Y = [0, 0.5, 0.7071067811865476, 0.8660254037844386, 1, 0]
    X = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]
    Y = [0.0, 1.0, 1.5849625007211563, 2.0, 2.321928094887362, 2.584962500721156, 2.807354922057604, 3.0, 3.1699250014423126, 3.3219280948873626, 3.4594316186372978, 3.5849625007211565, 3.700439718141092, 3.8073549220576037, 3.9068905956085187, 4.0]
    EPS = 0.1
    XX = 6.4
    file_read_from = ""
    file_write_to = ""
    debug = False

    def initialize_with(self, file, write_at):
        data_raw = reading_file(file)

        if data_raw is None:
            print("При чтении из файла произошла ошибка")

        self.file_read_from = file
        self.file_write_to = write_at

        self.N = data_raw['n']
        self.EPS = data_raw['eps']
        self.X = data_raw['vector_x']
        self.Y = data_raw['vector_y']
        self.XX = data_raw['xx']


    def get_b_j(self, points, xx):
        stepen = len(points) - 1
        sum = 0

        for i in range(0, stepen + 1):
            point = points[i]
            index = self.X.index(point)
            y = self.Y[index]
            sum += y / self.get_subfactorial_without_i(points, i)

        if self.debug:
            print('b_j: {}'.format(sum))
        return sum

    def get_subfactorial_without_i(self, points, index):
        subfactor = 1
        for i in range(0, index):
            subfactor *= (points[index] - points[i])
        for i in range(index + 1, len(points)):
            subfactor *= (points[index] - points[i])
        if self.debug:
            print('get_subfactorial_without_i: {}'.format(subfactor))
        return subfactor

    def get_subfactorial(self, points, xx):
        subfactor = 1
        for point in points[:-1]:
            subfactor *= (xx - point)
        if self.debug:
            print('subfactor: {}'.format(subfactor))
        return subfactor

    def get_n_n(self, points, xx):
        result = self.get_b_j(points, xx) * self.get_subfactorial(points, xx)
        if self.debug:
            print('n_n: {}'.format(result))
        return result

    def get_closest_index(self, vector, x):
        difference = 10e+10
        index = -1
        for element in vector:
            if difference > abs(element - x):
                difference = abs(element - x)
                index = vector.index(element)
        return index

    def remove_point_by_index_from_vectors(self, vector_x, vector_y, index):
        del vector_x[index]
        del vector_y[index]

    # def get_newton_j(self, points, xx):
    #     points.sort()
    #     stepen = len(points) - 1
    #     sum = 0
    #
    #     # for i in range(0, stepen + 1):
    #     sum += self.get_n_n(points, xx)
    #
    #     return sum

    def calculations(self):
        # проверяем на совпадение XX и вектора x
        if self.XX in self.X:
            # такая точка интерполирования уже есть
            index = self.X.index(self.XX)
            print("Интерполирование завершено успешно")
            return self.save(code=no_errors(), answer=self.Y[index], index=1)

        validation = validator.validated(self.XX, self.N, self.X, self.Y)

        if validation != "Norm":
            self.save(code=validation)
            print("Интерполирование завершено с ошибкой:", validation.error_message)
            return

        cur_x = self.X.copy()
        cur_y = self.Y.copy()
        points = []
        state = 0
        # -1 - расходится
        # 1 - успех
        # -2 - не достаточно точек(требуемая точность не достигнута)
        prev_newton = 0
        newton = 0
        prev_diff = 0

        n = 0
        while len(cur_x) > 0 and state == 0:
            if self.debug:
                print('-----iter: {}-----'.format(n))
            cur_point_index = self.get_closest_index(cur_x, self.XX)
            points.append(cur_x[cur_point_index])
            self.remove_point_by_index_from_vectors(cur_x, cur_y, cur_point_index)
            # строим многочлен n-ной степени по точкам points
            cur_newton = self.get_n_n(points, self.XX)
            # cur_diff = abs(cur_newton)

            if n > 1:
                # проверяем на погрешность
                # проверяем, что newton уменьшается
                if abs(cur_newton) > abs(prev_newton):
                    state = -1
                    print("Модуль разности между двумя последовательными интерполяционными значениями начал расти")
                    return self.save(code=week_accurancy())
                elif self.EPS > abs(cur_newton):
                    state = 1
                    print("Интерполирование завершено успешно")
                    return self.save(code=no_errors(), answer=newton, index=n)
                # elif self.EPS > abs(cur_newton):
                #     state = 1

            if self.debug:
                print('Cur newton: {}'.format(cur_newton))
                print('Prev newton: {}'.format(prev_newton))
            # print('cur_diff: {}'.format(cur_diff))
            # print('prev_diff: {}'.format(prev_diff))
            newton += cur_newton
            # prev_diff = cur_diff
            prev_newton = cur_newton
            n += 1

        if state == 0 and len(cur_x) == 0:
            state = -2
            print("Требуемая точность не достигнута")
            return self.save(code=not_enough_error())

        # switch by state
        return newton, state

    def save(self, code=None, answer=0, index=-1):
        if len(self.X) == 0:
            print("not initialized")
        file_saving = open(self.file_write_to, "w")
        file_saving.write("Код:" + code.error_code)
        file_saving.write('\n')
        if code.error_code == errors['awesome']['code']:
            file_saving.write("x = " + str(self.XX) + "\n")
            file_saving.write("N(x) = " + str(answer) + "\n")
            file_saving.write("Построен многочлен степени " + str(index - 1) + "\n")
        else:
            file_saving.write("Причина остановки: " + code.error_message + "\n")
        file_saving.close()


# calc = Calculations()
# calc.calculations()
# print('result: {}'.format(result))
