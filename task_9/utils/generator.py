import random


def get_array_item_str(array_item):
    if array_item[0] == 0:
        return "{}".format(array_item[1])
    else:
        return "{}*x^{}".format(array_item[1], array_item[0])


def print_array(array):
    out_str = ""
    out_str_array = []
    for elem in array:
        out_str_array.append(get_array_item_str(elem))

    out_str = out_str_array[0]
    for i in range(1, len(out_str_array)):
        if out_str_array[i][0] == "-":
            out_str += " - {}".format(out_str_array[i][1:])
        else:
            out_str += " + {}".format(out_str_array[i])

    return out_str


def calculate_in_x(array, x):
    sum = 0
    for elem in array:
        sum += elem[1] * x**elem[0]

    return sum


def generate_vector_x(count, start, finish):
    raz = (finish - start) / count
    vector = []
    for i in range(0, count):
        vector.append(start + i*raz + random.randint(1, raz))

    return vector



degree = 3
array = []
constant = 5
count = 5
XX = -3.5
start = -5
finish = 5
file_number = 11
eps = 0.1

for i in range(0, degree + 1):
    cur_elem = (i, random.randint(-constant, constant))
    array.append(cur_elem)


print("array: {}".format(array))
print("print: {}".format(print_array(array)))

vector_x = generate_vector_x(count, start, finish)
vector_y = []

print("vector: {}".format(vector_x))
for i in range(0, count):
    vector_y.append(calculate_in_x(array, vector_x[i]))

print("[vectorX]: {}".format(' '.join(str(x) for x in vector_x)))
print("[vectorY]: {}".format(' '.join(str(x) for x in vector_y)))
print("[XX]: {}".format(XX))
print("[YY]: {}".format(calculate_in_x(array, XX)))


file = open("test_{}.txt".format(file_number), "w")
file.write("[EPS]: {}\n".format(eps))
file.write("[XX]: {}\n".format(XX))
file.write("[N]: {}\n".format(count))
file.write("[vectorX]: {}\n".format(' '.join(str(x) for x in vector_x)))
file.write("[vectorY]: {}\n".format(' '.join(str(x) for x in vector_y)))


