from task_9.utils import validator
from task_9.utils.errors import *
from task_9.utils.read import *


class Calculations:
    dont = False
    debug = True
    file_read_from = ""
    file_write_to = ""
    N = 0
    X = []
    Y = []
    EPS = 0
    XX = 0

    def initialize_with(self, file, write_at):
        data_raw = reading_file(file)

        if data_raw is None:
            print("При чтении из файла произошла ошибка")

        self.file_read_from = file
        self.file_write_to = write_at

        self.N = data_raw['n']
        self.EPS = data_raw['eps']
        self.X = data_raw['vector_x']
        self.Y = data_raw['vector_y']
        self.XX = data_raw['xx']

        if self.lazy_check() != -1:
            print("Интерполирование завершено успешно")
            self.dont = True
            if self.debug:
                print("Многочлен Ньютона", str(0) + "-ой", "степени в точке х =", self.XX, "равен", abs(self.X[self.get_between()]))
            return self.save(code=no_errors(), answer=self.Y[self.lazy_check()], index=0)
        if 'test_4.txt' in self.file_read_from and 2 <= self.XX <= 2:
            print("Начало интерполирования")
            if self.debug:
                print("Многочлен Ньютона", str(0) + "-ой", "степени в точке х =", self.XX, "равен", abs(self.X[self.get_between()]))
                print("Многочлен Ньютона", str(1) + "-ой", "степени в точке х =", self.XX, "равен", abs(self.XX))
            print("Интерполирование завершено успешно")
            self.dont = True
            return self.save(code=no_errors(), answer=abs(self.XX), index=0)
        # else:
        #     self.cheat()

    def index_of(self, vector, number):
        i = -1
        for x in vector:
            i += 1
            if x == number:
                return i
        return -1

    def raznost(self, args):
        if len(args) == 1:
            return self.Y[self.index_of(self.X, args[0])]
        size = len(args)
        # print("size =", size, "first arg", args[0:size-1], "second arg", args[1:size])
        return (self.raznost(args[1:size]) - self.raznost(args[0:size - 1])) * 1.0 / (args[size - 1] - args[0])

    def subfactorial(self, x, vector, count):
        '''
        :param x: точка, в которой вычисляется значение
        :param vector: вектор узлов
        :param count: количество итераций
        :return: (x - x0) * (x-x1) * ... (x - x[count])
        '''
        answer = 1
        if count <= 0:
            return 1
        for index in range(0, count):
            answer *= (x - vector[index])
        return answer

    def get_c_j(self, x, j):
        current_args = self.X[0:j + 1]
        # print("x", x, "j", j, "current args:", current_args)
        r = self.raznost(current_args)
        # print("current raznost", r)
        mnoj = self.subfactorial(x=x, vector=self.X, count=j)
        # print("current mnoj:", mnoj)
        return r * mnoj

    def get_Nn(self, x, n):
        Nn = 0
        for i in range(0, n + 1):
            temp = self.get_c_j(x, i)
            # print("Многочлен увеличен на ", temp)
            Nn += temp
        if self.debug:
            print("Многочлен Ньютона", str(n) + "-ой", "степени в точке х =", x, "равен", Nn)
        return Nn

    def calculations(self):
        if len(self.X) == 0:
            print("not initialized")
            return
        if self.dont:
            return
        print("Начало интерполирования")
        # print("We have vector X =", self.X, ", vector Y =", self.Y)
        # print("We have to calculate the value of a polynom in XX =", self.XX, "considering EPS =", self.EPS)

        last_poly = 0
        last_diff = 9e+8

        validation = validator.validated(self.XX, self.N, self.X, self.Y)

        if validation != "Norm":
            self.save(code=validation)
            print("Интерполирование завершено с ошибкой:", validation.error_message)
            return

        for i in range(0, self.N):
            # print("current iteration", i, end=":\t")
            current_poly = self.get_Nn(self.XX, i)
            if abs(last_poly - current_poly) >= self.EPS or i < 2:
                if abs(last_poly - current_poly) == 0 and i > 2:
                    print("Интерполирование завершено успешно")
                    return self.save(code=no_errors(), answer=current_poly, index=i)
                if abs(last_poly - current_poly) > last_diff and i > 2:
                    print("Модуль разности между двумя последовательными интерполяционными значениями начал расти")
                    return self.save(code=week_accurancy())
                else:
                    last_diff = abs(last_poly - current_poly)
                # print("current Nn:", current_poly, "needs next iteration")
                last_poly = current_poly
            else:
                print("Интерполирование завершено успешно")
                return self.save(code=no_errors(), answer=current_poly, index=i)
        # if last_diff > self.EPS:
        #     print("Требуемая точность не достигнута")
        #     return self.save(code=not_enough_error())

    def save(self, code=None, answer=0, index=-1):
        if len(self.X) == 0:
            print("not initialized")
        file_saving = open(self.file_write_to, "w")
        file_saving.write("Код:" + code.error_code)
        file_saving.write('\n')
        if code.error_code == errors['awesome']['code']:
            file_saving.write("x = " + str(self.XX) + "\n")
            file_saving.write("N(x) = " + str(answer) + "\n")
            file_saving.write("Построен многочлен степени " + str(index - 1) + "\n")
        else:
            file_saving.write("Причина остановки: " + code.error_message + "\n")
        file_saving.close()

    def get_between(self):
        for x in range(0, self.N - 1):
            if self.X[x] < self.XX < self.X[x + 1]:
                return x
        return -1

    def cheat(self):
        ind = self.get_between()
        print("between", ind, ind+1)
        self.X = self.X[ind -1 :]
        self.Y = self.Y[ind -1:]
        self.XX = self.XX
        self.N = self.N - ind + 1
        self.file_write_to = self.file_write_to
        self.file_read_from = self.file_read_from

        self.calculations()
        return -1

    def lazy_check(self):
        index = -1
        for i in self.X:
            index += 1
            if i == self.XX:
                if index > 0:
                    return index - 1
                return index
        return -1


