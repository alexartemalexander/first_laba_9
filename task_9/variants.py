# Первый вариант

x = [1, 3, 5, 7]  # x-координаты из таблицы
y = [2, 3, 3, 2]  # y-координаты из таблицы


def product(val, n):
    """ Вспомогательный генератор для вычисления произведения разностей координат """
    mul = 1
    for i in range(n):
        if i: mul *= val - x[i - 1]
        yield mul


C = []  # список коэффициентов полинома

# вычисляем коэффициенты
for n in range(len(x)):
    p = product(x[n], n + 1)
    C.append((y[n] - sum(C[k] * next(p) for k in range(n))) / next(p))


def f(v):
    """ Значение полинома в точке v """
    return sum(C[k] * p for k, p in enumerate(product(v, len(C))))
