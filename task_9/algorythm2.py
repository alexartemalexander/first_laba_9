
class Calculations:
    step = 1.0
    vectorX = [0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0]
    vectorY = [0, 0, 2, 6, 12, 20, 30, 42]

    def delta(self, p, i):
        if p == 1:
            return self.vectorY[i] - self.vectorY[i - 1]
        return self.delta(p - 1, i) - self.delta(p - 1, i - 1)

    def factorial(self, value):
        if value == 0:
            return 1
        return value * self.factorial(value - 1)

    def get_c_j(self, index_j):
        if index_j == 0:
            return self.vectorY[0]
        return self.delta(index_j, index_j) * 1.0 / (self.factorial(index_j) * (self.step ** index_j))

    def get_P_n(self, x):
        length = len(self.vectorY)
        result = self.get_c_j(0)
        for j in range(1, length):
            print("current iteration ", j)
            current_term = self.get_c_j(j)
            print("Cj = ", current_term)
            for k in range(0, j):
                print("current term ", current_term, end='   ')
                current_term *= x - self.vectorX[k]
            print()
            print(" final current term ", current_term)
            result += current_term
            print('-----------------------------------')
        return result

    def start_calculations_in_point(self, point):
        self.step = self.vectorX[1] - self.vectorX[0]
        result_in_point = self.get_P_n(point)
        return result_in_point


calcs = Calculations()
the_point = 3.0
answer = calcs.start_calculations_in_point(the_point)
print('Vector X: ', calcs.vectorX)
print('Vector Y: ', calcs.vectorY)
print('result in point ', the_point, ' is ', answer)
print("Epsilon: ", calcs.vectorY[3] - answer)
